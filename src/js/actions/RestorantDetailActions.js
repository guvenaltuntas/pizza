import axios from "axios";
import { getTokenAnd } from "./TokenActions"

export function getRestorantDetail(id) {
  return (dispatch) => {
    dispatch({type: "GET_RESTORANT_DETAILS"})
    getTokenAnd((token)=>{
      axios.get("https://mockapi.pizza.de/v1/restaurants/"+id,{ 
        headers: { token: token }
      }).then((response) => {
        dispatch({type: "GET_RESTORANT_DETAILS_FULFILLED", payload: response.data})
      }).catch((err)=>{
        dispatch({type: "GET_RESTORANT_DETAILS_REJECTED", payload: err})
      })
    });
  }
}