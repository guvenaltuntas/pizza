import axios from "axios";
var token = null;
export function getTokenAnd(callBack) {
  if(token){
    callBack(token);
  }else{
    axios.get("https://mockapi.pizza.de/v1/auth").then((response) => {
        token = response.data.token
        callBack(response.data.token);
    })
  }
}