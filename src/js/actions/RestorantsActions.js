import axios from "axios";
import { getTokenAnd } from "./TokenActions"

export function getRestorants() {
  return (dispatch) => {
    dispatch({type: "GET_RESTORANTS"})
    getTokenAnd((token)=>{
      axios.get("https://mockapi.pizza.de/v1/restaurants",{ 
        headers: { token: token }
      }).then((response) => {
        dispatch({type: "GET_RESTORANTS_FULFILLED", payload: response.data})
      }).catch((err)=>{
        dispatch({type: "GET_RESTORANTS_REJECTED", payload: err})
      })
    });
  }
}