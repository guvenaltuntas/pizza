import React from "react";
import { Link } from "react-router";

export class Header extends React.Component {
    navigateHome(){
        this.props.navigation.navigate("/");
    }

    render() {
        return (
            <header>
                <h1><a href="/" onClick={this.navigateHome.bind(this)} class="project-logo">Pizza</a></h1>
            </header>
        );
    }
}
