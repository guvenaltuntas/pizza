import React from "react";
import { Link } from "react-router";
import { ProductListItem } from "./ProductListItem"

export class RestorantSection extends React.Component {
  render() {
    const data = this.props.data;

    const sectionProducts = data.items.map((product)=><ProductListItem data={product}></ProductListItem>);
    return (
        <div class="restorant-section">
            <h3>{data.name}</h3>
            <ul class="products">{sectionProducts}</ul>
        </div>
    );
  }
}
