import React from "react";
import { Link } from "react-router";

export class ProductListItem extends React.Component {
    logProductData(){
        console.log(this.props.data);
    }
    render() {
        const data = this.props.data;
        return (
            <li>
                {data.name}
                <button class="btn" onClick={this.logProductData.bind(this)}>+</button>
                <span class="price">{data.price}</span>
            </li>
        );
    }
}