import React from "react";
import { Link } from "react-router";

export class RestorantItem extends React.Component {
  render() {
    const data = this.props.data;
    const categories = data.general.categories[0].split(",").filter((value, index, array)=>{
        return array.indexOf(value) == index;
    }).map(category=><li>{category}</li>);
    return (
        <Link to={`/restorant/${data.id}`} class="restorant-item clearfix">
            <div class="logo"><img src={data.general.logo_uri} /></div>
            <h2>{data.general.name} <strong>({data.rating.average})</strong></h2>
            <address>{data.address.city} / {data.address.street_name} {data.address.street_number}</address>
            <ul>{categories}</ul>
        </Link>
    );
  }
}
