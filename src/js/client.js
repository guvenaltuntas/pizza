import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux"
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Layout from "./pages/Layout";
import RestorantDetail from "./pages/RestorantDetail";
import Restorants from "./pages/Restorants";

import store from "./store"

import "../scss/Combined.scss"

const app = document.getElementById('app');

ReactDOM.render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={Layout}>
        <IndexRoute component={Restorants}></IndexRoute>
        <Route path="restorant(/:id)" name="restorant" component={RestorantDetail}></Route>
      </Route>
    </Router>
  </Provider>,
app);
