export default function reducer(state={
  restorantData: {
    info:{
      categories:[]
    },
    rating:{},
    address:{},
    sections:[]
  },
  fetching: false,
  fetched: false,
  error: null,
}, action) {

  switch (action.type) {
    case "GET_RESTORANT_DETAILS": {
      return {...state, fetching: true}
    }
    case "GET_RESTORANT_DETAILS_REJECTED": {
      return {...state, fetching: false, error: action.payload}
    }
    case "GET_RESTORANT_DETAILS_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        restorantData: action.payload
      }
    }
  }

  return state
}
