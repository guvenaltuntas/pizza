export default function reducer(state={
    restorants: [],
    pagination:{
      total_items: 0,
      total_pages: 1,
      limit: 100,
      page: 1,
      offset: 0
    },
    fetching: false,
    fetched: false,
    error: null,
  }, action) {

    switch (action.type) {
      case "GET_RESTORANTS": {
        return {...state, fetching: true}
      }
      case "GET_RESTORANTS_REJECTED": {
        return {...state, fetching: false, error: action.payload}
      }
      case "GET_RESTORANTS_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          restorants: action.payload.data,
          pagination: action.payload.pagination
        }
      }
    }

    return state
}
