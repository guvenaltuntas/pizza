import { combineReducers } from "redux"
import restorants from "./restorantsReducer"
import restorantDetail from "./restorantDetailReducer"

export default combineReducers({
    restorants,
    restorantDetail
})