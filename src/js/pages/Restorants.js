import React from "react";
import { getRestorants } from "../actions/RestorantsActions"
import { connect } from "react-redux"
import { RestorantItem } from "../components/RestorantItem"

@connect((store)=>{
  return {
    restorants: store.restorants.restorants,
    restorantsFetching: store.restorants.fetching,
    pagination: store.restorants.pagination
  }
})
export default class Restorants extends React.Component {
  constructor(){
    super();
    this.state = {
      filter : "",
      sorting: "",
      startIndex: 0
    }
  }

  totalFilteredRestorantCount = 0;

  componentWillMount(){
    this.props.dispatch(getRestorants());
    this.setState({...this.state, 
      filter: this.props.location.query.filter||this.state.filter, 
      sorting: this.props.location.query.sorting||this.state.sorting, 
      startIndex: parseInt(this.props.location.query.startIndex)||this.state.startIndex
    })
  }

  filterChanged(e){
    this.setState({...this.state, filter: e.target.value, startIndex: 0})
    this.props.history.replaceState(null,"/?filter="+e.target.value+"&sorting="+this.state.sorting+"&startIndex=0");
  }

  sortingChanged(e){
    this.setState({...this.state, sorting: e.target.value})
    this.props.history.replaceState(null,"/?filter="+this.state.filter+"&sorting="+e.target.value+"&startIndex="+this.state.startIndex);
  }

  changePage(pageNum){
    var newStartIndex = (pageNum-1)*this.props.pagination.total_pages;
    this.setState({...this.state, 
      startIndex: newStartIndex
    })
    this.props.history.replaceState(null,"/?filter="+this.state.filter+"&sorting="+this.state.sorting+"&startIndex="+newStartIndex);
  }

  prevPage(){
    if(this.state.startIndex>0){
      var newPage = (this.state.startIndex/this.props.pagination.total_pages);
      this.changePage(newPage);
    }
  }

  nextPage(){
    if(this.state.startIndex+this.props.pagination.total_pages<this.totalFilteredRestorantCount){
      var newPage = (this.state.startIndex/this.props.pagination.total_pages)+2;
      this.changePage(newPage);
    }
  }

  render() {

    const { restorants , pagination, restorantsFetching } = this.props;

    const categoryList = [].concat.apply([], restorants.map((restorant)=>{
      return restorant.general.categories[0].split(",");
    })).filter((value, index, array)=>{
      return array.indexOf(value) == index;
    });

    const filterOptions = categoryList.map((category)=><option value={category}>{category}</option>);

    const restorantList = restorants.filter((restorant)=>{
      if(this.state.filter==""){
        return true;
      }
      return restorant.general.categories[0].split(",").indexOf(this.state.filter)>=0;
    }).sort((a, b) => {
      switch(this.state.sorting){
        case "atoz":{
          return a.general.name > b.general.name;
        }
        case "ztoa":{
          return a.general.name < b.general.name;
        }
        case "hrf":{
          return a.rating.average < b.rating.average;
        }
        case "lrf":{
          return a.rating.average > b.rating.average;
        }
        case "hmovf":{
          return a.min_order_value < b.min_order_value;
        }
        case "lmovf":{
          return a.min_order_value > b.min_order_value;
        }
      }
    }).map(restorant=><RestorantItem data={restorant}></RestorantItem>);

    this.totalFilteredRestorantCount = restorantList.length;
    const pageRestorantList = restorantList.splice(this.state.startIndex,this.props.pagination.total_pages)
    
    const pageCount = this.totalFilteredRestorantCount%pagination.total_pages>0? parseInt(this.totalFilteredRestorantCount/pagination.total_pages)+1: parseInt(this.totalFilteredRestorantCount/pagination.total_pages);
    const activeIndex = parseInt(this.state.startIndex)/this.props.pagination.total_pages;
    const paginationLinks = Array.from(Array(pageCount).keys()).map((item)=><li class={item==activeIndex?'active':''} onClick={() => this.changePage(item+1)}>{item+1}</li>);
    
    if(this.props.restorantsFetching){
      return(
        <div class="loading"></div>
      )
    }

    return (
      
      <div id="restorantListContent">
        <div class="filters">
          <select onChange={this.filterChanged.bind(this)} value={this.state.filter}>
            <option value="">Select Filter</option>
            {filterOptions}
          </select>
          <select onChange={this.sortingChanged.bind(this)} value={this.state.sorting}>
            <option value="">Select Sorting</option>
            <option value="atoz">A to Z</option>
            <option value="ztoa">Z to A</option>
            <option value="hrf">High Rating First</option>
            <option value="lrf">Low Rating First</option>
            <option value="hmovf">High Minimum Order Value First</option>
            <option value="lmovf">Low Minimum Order Value First</option>
          </select>
        </div>
        <section id="restorantList">
          {pageRestorantList}
        </section>
        <ul class="pagination">
          <li onClick={this.prevPage.bind(this)}>Back</li>
          {paginationLinks}
          <li onClick={this.nextPage.bind(this)}>Next</li>
        </ul>
      </div>
    );
  }
}
