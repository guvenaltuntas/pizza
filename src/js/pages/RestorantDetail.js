import React from "react";
import { getRestorantDetail } from "../actions/RestorantDetailActions"
import { connect } from "react-redux"
import { RestorantSection } from "../components/RestorantSection"

@connect((store)=>{
  return {
    restorantData: store.restorantDetail.restorantData,
    restorantFetching: store.restorantDetail.fetching
  }
})
export default class RestorantDetail extends React.Component {
  componentWillMount(){
    this.props.dispatch(getRestorantDetail(this.props.params.id));
  }
  render() {
    const { restorantData, restorantFetching } = this.props;

    const categories = restorantData.info.categories.filter((value, index, array)=>{
        return array.indexOf(value) == index;
    }).map(category=><li>{category}</li>);

    const productSections = restorantData.sections.map((section)=><RestorantSection data={section}></RestorantSection>);
    
    if(restorantFetching){
      return (
        <div class="loading"></div>
      )
    }

    return (
      <div>
        <section id="restorantInfo" class="restorant-item clearfix">
            <div class="logo"><img src={restorantData.info.logoUri} /></div>
            <h2>{restorantData.info.name} <strong>({restorantData.rating.average})</strong></h2>
            <address>{restorantData.address.city} / {restorantData.address.streetName} {restorantData.address.streetNumber}</address>
            <ul>{categories}</ul>
        </section>
        <section id="restorantSections">{productSections}</section>
      </div>
    );
  }
}
