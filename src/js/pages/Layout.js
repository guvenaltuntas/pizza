import React from "react";
import { Header } from "../components/Header"

export default class Layout extends React.Component {
  render() {
    return (
      <div id="layout" class="project-size">
          <Header></Header>
          <section id="content">{this.props.children}</section>
      </div>
    );
  }
}
