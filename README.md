* Pizza
* Version 0.0.0

### Requirements ###

* NodeJS
* available 8080 port

### Setup ###

* go to the folder you want to locate project exp: cd /
* create a projet folder exp: sudo mkdir #FOLDER_NAME#
* get ownership of folder exp: sudo chown #YOUR_USER_NAME# #FOLDER_NAME#
* go to the folder you created exp: cd #FOLDER_NAME#
* clone repo from bitbucket exp: git clone https://guvenaltuntas@bitbucket.org/guvenaltuntas/pizza.git .   (last dot is nessesery for not creating another folder)
* run > npm install
* run > npm run dev
* open https://localhost:8080 in your browser and grant it priviliges for using https without a ssl certificate